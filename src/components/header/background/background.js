import { Component } from "react";

import background from "../../../assets/images/background.png";

class Background extends Component {
    render() {
        return (
            <div>
                <img src={background} alt="background" width={800}/>
            </div>
        )
    }
}

export default Background;