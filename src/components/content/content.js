import { Component, useState } from "react";
import Input from "./input/input";
import Output from "./output/output";

const Content = () => {
    const [inputTextMessage, setInputMessage] = useState("");
    const [outputMessage, setOutputMessage] = useState([]);
    const [displayLike, setDisplayLike] = useState(false);

    const saveInputText = (message) =>{
        setInputMessage(message)
    }

    const outputState = () => {
        console.log (inputTextMessage)
        setOutputMessage([...outputMessage, inputTextMessage]);
        setDisplayLike(true);
    }

    return (
        <>
            <Input 
                inputTextMessageProp = {inputTextMessage}
                saveInputTextProp = {saveInputText}
                outputStateProp = {outputState}
            />
            <Output 
                outputTextMessageProp = {outputMessage}
                displayLikeProp = {displayLike}
            />
        </>
    )
}

export default Content;
/*
class Content extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputTextMessage : "Nhập gì đó vào đây",
            outputMessage: [],
            displayLike: false
        }
    }

    saveInputText = (message) =>{
        this.setState({
            inputTextMessage: message,
        })
    }

    outputState = () => {
        console.log (this.state.inputTextMessage)
        this.setState({
            outputMessage: [...this.state.outputMessage, this.state.inputTextMessage],
            displayLike: true
        })
    }

    render() {
        return (
            <>
                <Input 
                    inputTextMessageProp = {this.state.inputTextMessage}
                    saveInputTextProp = {this.saveInputText}
                    outputStateProp = {this.outputState}
                />
                <Output 
                    outputTextMessageProp = {this.state.outputMessage}
                    displayLikeProp = {this.state.displayLike}
                />
            </>
        )
    }
}

export default Content;
*/