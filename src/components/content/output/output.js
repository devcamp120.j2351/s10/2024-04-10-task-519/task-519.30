import { Component } from "react";

import like from "../../../assets/images/like.png";
const Output = ({outputTextMessageProp, displayLikeProp}) => {
    return (
        <>
            <div className="pt-4">
                <p>{outputTextMessageProp.map ((value, key) => {
                    return <p id = {key}>{value}</p>
                })}</p>
            </div>
            <div>
                <img src={like} alt="like" width={100} 
                style = {{display: displayLikeProp == true? "block": "none"}}/>
            </div>
        </>
    )
}

export default Output;
/*
class Output extends Component {
    render() {
        return (
            <>
                <div className="pt-4">
                    <p>{this.props.outputTextMessageProp.map ((value, key) => {
                        return <p id = {key}>{value}</p>
                    })}</p>
                </div>
                <div>
                    <img src={like} alt="like" width={100} 
                    style = {{display: this.props.displayLikeProp == true? "block": "none"}}/>
                </div>
            </>
        )
    }
}

export default Output;
*/