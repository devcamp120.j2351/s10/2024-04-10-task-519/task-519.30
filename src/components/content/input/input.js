import { Component } from "react";
import { Button, Container, Row, Col, Input} from 'reactstrap';

const MyInput = ({ inputTextMessageProp, saveInputTextProp, outputStateProp }) => {

    const onInputChangeHandler = (event) => {
        //console.log("Giá trị của ô input");
        //console.log(event.target.value);
        saveInputTextProp(event.target.value);
    }

    const onButtonClickHandler = () => {
        console.log("Nút gửi thông điệp được bấm");
        outputStateProp();
    }
    return (
        <Container>
            <Row className = "mt-4">
                <Col md="12">
                    <label>Message cho bạn 12 tháng tới:</label>
                </Col>
                <Col md="12">
                    <Input onChange={onInputChangeHandler}
                        className="form-control"
                        value={inputTextMessageProp}
                    />

                    <Button className="btn btn-success mt-4"
                        onClick={onButtonClickHandler}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default MyInput;
/*
class Input extends Component {

    onInputChangeHandler = (event) => {
        //console.log("Giá trị của ô input");
        //console.log(event.target.value);
        this.props.saveInputTextProp(event.target.value);
    }

    onButtonClickHandler = () => {
        console.log("Nút gửi thông điệp được bấm");
        this.props.outputStateProp();
    }

    render() {
        return (
            <Container>
                <Row>
                    <div className="pt-4">
                        <label>Message cho bạn 12 tháng tới:</label>
                    </div>
                </Row>
                <Row>
                    <div>
                        <input onChange={this.onInputChangeHandler} className="form-control"
                            value={this.props.inputTextMessageProp}
                        ></input>
                    </div>
                </Row>
                <Row>
                    <div>
                        <button className="btn btn-success mt-4" onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </Row>
            </Container>
        )
    }
}

export default Input;
*/